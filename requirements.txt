Flask==0.12
Flask-SQLAlchemy==2.1
Flask-Migrate==2.0.3
gunicorn==19.6.0
